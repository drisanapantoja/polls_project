from polls_project.math_operations import sum_operation


def test_sum_1_2_should_be_3():
    # given
    a = 1
    b = 2
    expected = 3
    sum_object = sum_operation.SumOperation()
    # when
    result = sum_object.sum(a, b)
    # then
    assert result == expected


def test_div_10_2_should_be_5():
    a = 10
    b = 2
    expected = 5
    div_object = sum_operation.DivOperation()
    result = div_object.div(a, b)
    assert result == expected
