from polls_project.math_operations.times_operation import TimesOperation


def test_times_1_2_should_be_2():
    # given
    a = 1
    b = 2
    expected = 2
    times_object = TimesOperation()
    # when
    result = times_object.times(a, b)
    # then
    assert result == expected
